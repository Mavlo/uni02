/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jugador.dao;

import com.mycompany.jugador.dao.exceptions.NonexistentEntityException;
import com.mycompany.jugador.dao.exceptions.PreexistingEntityException;
import com.mycompany.jugador.entity.Inscripcionjugador;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Pablo
 */
public class InscripcionjugadorJpaController implements Serializable {

    public InscripcionjugadorJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Inscripcionjugador inscripcionjugador) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(inscripcionjugador);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findInscripcionjugador(inscripcionjugador.getRut()) != null) {
                throw new PreexistingEntityException("Inscripcionjugador " + inscripcionjugador + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Inscripcionjugador inscripcionjugador) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            inscripcionjugador = em.merge(inscripcionjugador);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = inscripcionjugador.getRut();
                if (findInscripcionjugador(id) == null) {
                    throw new NonexistentEntityException("The inscripcionjugador with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Inscripcionjugador inscripcionjugador;
            try {
                inscripcionjugador = em.getReference(Inscripcionjugador.class, id);
                inscripcionjugador.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The inscripcionjugador with id " + id + " no longer exists.", enfe);
            }
            em.remove(inscripcionjugador);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Inscripcionjugador> findInscripcionjugadorEntities() {
        return findInscripcionjugadorEntities(true, -1, -1);
    }

    public List<Inscripcionjugador> findInscripcionjugadorEntities(int maxResults, int firstResult) {
        return findInscripcionjugadorEntities(false, maxResults, firstResult);
    }

    private List<Inscripcionjugador> findInscripcionjugadorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Inscripcionjugador.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Inscripcionjugador findInscripcionjugador(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Inscripcionjugador.class, id);
        } finally {
            em.close();
        }
    }

    public int getInscripcionjugadorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Inscripcionjugador> rt = cq.from(Inscripcionjugador.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
